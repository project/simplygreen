<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <!--[if IE 6]><link rel="stylesheet" href="<?php echo $base_path . $directory; ?>/style.ie6.css" type="text/css" /><![endif]-->
  <!--[if IE 7]><link rel="stylesheet" href="<?php echo $base_path . $directory; ?>/style.ie7.css" type="text/css" media="screen" /><![endif]-->
</head>

<body<?php print phptemplate_body_class($left, $right); ?>>
<div id="header">
	<div class="column">
		<?php if ($logo): ?>
			<div id="logo"><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a></div>
		<?php endif; ?>
		<div class="logo">
			<?php if ($site_name) { ?><h1 class="logo-name"><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
			<?php if ($site_slogan) { ?><div class='logo-text'><?php print $site_slogan ?></div><?php } ?>
		</div>
	</div>
</div>

<?php if (!empty($navigation)): ?>
	<div class="nav">
		<div class="column">
			<div class="l"></div>
			<div class="r"></div>
			<?php echo $navigation; ?>
		</div>
	</div>
<?php endif; ?>

<div id="page">
	<div id="container">
		<div class="column">
		<?php if ($left): ?>
			<div id="sidebar-left" class="sidebar">
				<?php print $left ?>
			</div>
		<?php endif; ?>

		<div id="main"><div class="content-padding">
			<?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
			<?php if (!$is_front) print $breadcrumb ?>
			<?php if ($show_messages) { print $messages; } ?>
			<?php if ($tabs) { ?><div class="tabs"><?php print $tabs ?></div><?php } ?>
			<?php if ($title) { ?><h1 class="title"><?php print $title ?></h1><?php } ?>
			<?php print $help ?>
			<?php print $content; ?>
			<?php print $feed_icons; ?>
		</div></div>

		<?php if ($right): ?>
			<div id="sidebar-right" class="sidebar">
				<?php print $right ?>
			</div>
		<?php endif; ?>
		<div style="clear: both;"> </div>
		</div>
	</div>
</div>

<div id="footer">
	<div class="column">
		<div id="postcontent"><?php print $footer ?><div style="clear: both;"> </div></div>
		<?php print $footer_message ?>
	</div>
</div>

<?php print $closure ?>
</body>
</html>